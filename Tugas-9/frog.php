<?php
require_once 'animal.php';

class Frog extends Animal{
    public function getLegs()
    {
        return 2;
    }

    public function jump()
    {
        return "hop hop";
    }
}
?>
