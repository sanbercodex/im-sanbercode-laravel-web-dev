<?php
require_once 'animal.php';
require_once 'Ape.php';
require_once 'Frog.php';

echo "<h2>latihan OOP</h2>";

$sheep = new Animal("shaun");
echo "Name : " . $sheep->getName() . "<br>"; // "shaun"
echo "Legs : " . $sheep->getLegs() . "<br>"; // 4
echo "Cold Blooded : " . $sheep->getColdBlooded() . "<br><br>"; // "no"

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->getName() . "<br>"; // "kera sakti"
echo "Legs : " . $sungokong->getLegs() . "<br>"; // 4
echo "Cold Blooded : " . $sungokong->getColdBlooded() . "<br>"; // "no"
echo "Yell : " . $sungokong->yell() . "<br><br>"; // "Auooo"

$kodok = new Frog("buduk");
echo "Name : " . $kodok->getName() . "<br>"; // "buduk"
echo "Legs : " . $kodok->getLegs() . "<br>"; // 2
echo "Cold Blooded : " . $kodok->getColdBlooded() . "<br>"; // "no"
echo "Jump : " . $kodok->jump() . "<br>"; // "hop hop"
?>
