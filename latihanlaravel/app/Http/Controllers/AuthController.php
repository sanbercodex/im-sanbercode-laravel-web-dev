<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function signup() {
        return view('page.register');
    }

    public function reg(Request $request) {
        $namaDepan = $request['name'];
        $namaBelakang = $request['last'];
        
        return view('page.welcome',['namaDepan' => $namaDepan,'namaBelakang' => $namaBelakang]);

    }
    //
}
