<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat acount baru!</h1>
<h3>Sign Up Form</h3>
<form action="/signup" method="POST">
    @csrf
    <label>First Name :</label><br><br>
    <input type="text" name="name"><br><br>
    <label>Last Name :</label><br><br>
    <input type="text" name="last"><br><br>
    <label>Gender :</label><br><br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br><br>
    <label>Nationality :</label><br><br>
    <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="singapura">Singapura</option>
        <option value="malaysia">Malaysia</option>
    </select><br><br>
    <label>Language Spoken :</label><br><br>
    <input type="checkbox" name="ind">Bahasa Indonesia<br>
    <input type="checkbox" name="eng">English<br>
    <input type="checkbox" name="oth">Other<br><br>
    <label>Bio:</label><br><br>
    <textarea name="bio" rows="10" cols="30"></textarea><br><br>

    <input type="submit" value="signup">

</form>

</body>
</html>