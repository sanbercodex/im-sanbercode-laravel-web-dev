<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>

<body>
    <h1>Berlatih Function PHP</h1>
    <?php

    echo "<h3> Soal No 1 Greetings </h3>";

    // Soal No 1
    function greetings($name)
    {
        echo "Halo " . ucfirst($name) . ", Selamat Datang di Sanbercode!";
    }

    // Hapus komentar untuk menjalankan code!
    greetings("Bagas");
    echo "<br>";
    greetings("Wahyu");
    echo "<br>";
    greetings("nama peserta");

    echo "<br>";

    echo "<h3>Soal No 2 Reverse String</h3>";

    // Soal No 2
    function reverseString($str)
    {
        $reverse = '';
        for ($i = strlen($str) - 1; $i >= 0; $i--) {
            $reverse .= $str[$i];
        }
        echo $reverse;
    }

    // Hapus komentar di bawah ini untuk jalankan Code
    reverseString("nama peserta");
    echo "<br>";
    reverseString("Sanbercode");
    echo "<br>";
    reverseString("We Are Sanbers Developers");
    echo "<br>";

    echo "<h3>Soal No 3 Palindrome </h3>";

    // Soal No 3 
    function palindrome($str)
    {
        $reverse = '';
        for ($i = strlen($str) - 1; $i >= 0; $i--) {
            $reverse .= $str[$i];
        }
        if ($str == $reverse) {
            echo "true";
        } else {
            echo "false";
        }
    }

    // Hapus komentar di bawah ini untuk jalankan code
    echo palindrome("civic") . "<br>"; // true
    echo palindrome("nababan") . "<br>"; // true
    echo palindrome("jambaban") . "<br>"; // false
    echo palindrome("racecar") . "<br>"; // true


    echo "<h3>Soal No 4 Tentukan Nilai </h3>";

    // Soal 4
    function tentukan_nilai($nilai)
    {
        if ($nilai >= 85 && $nilai <= 100) {
            return "Sangat Baik";
        } elseif ($nilai >= 70 && $nilai < 85) {
            return "Baik";
        } elseif ($nilai >= 60 && $nilai < 70) {
            return "Cukup";
        } else {
            return "Kurang";
        }
    }

    // Hapus komentar di bawah ini untuk jalankan code
    echo tentukan_nilai(98) . "<br>"; //Sangat Baik
    echo tentukan_nilai(76) . "<br>"; //Baik
    echo tentukan_nilai(67) . "<br>"; //Cukup
    echo tentukan_nilai(43) . "<br>"; //Kurang

    ?>

</body>

</html>
